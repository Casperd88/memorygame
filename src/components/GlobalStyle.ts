import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Gentium+Basic:400,700|Open+Sans:400,400i,600,600i,700,700i&display=swap');

  body {
    background: green;
    color: white;
    font-size: 16px;
    font-family: 'Open Sans', sans-serif;
    padding: 40px;
  }

  button, input, textarea, select {
    font-family: 'Open Sans', sans-serif;
  }

  * {
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
  }
`;
