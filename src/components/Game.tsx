import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Card from "./Card";
import { ICard, LevelType } from "./../types";
import { shuffleArray } from "./../utils";

const CARD_RANKS: ICard["rank"][] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

const GAME_LEVEL_CARD_MAPPING = {
  0: 2,
  1: 4,
  2: 8
};

const GameForm = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 40px;
  > * {
    margin: 5px;
  }
`;

const VictoryMessage = styled.h2`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.5);
  cursor: pointer;
  font-size: 10vw;
  font-weight: 700;
  font-family: "Gentium Basic", serif;
  text-shadow: 0.1em 0.1em 0.5em rgba(0, 0, 0, 0.5);
`;

const Title = styled.h1`
  font-weight: 700;
  font-family: "Gentium Basic", serif;
  font-size: 60px;
  margin-bottom: 40px;
  line-height: 1em;
`;

function getShuffledDeck(): ICard[] {
  const deck = ["clubs", "diamonds", "hearts", "spades"].reduce<ICard[]>(
    (result, suit) => {
      return [
        ...result,
        ...CARD_RANKS.map(rank => {
          return {
            suit,
            rank
          } as ICard;
        })
      ];
    },
    []
  );
  shuffleArray(deck);
  return deck;
}

const Game: React.FunctionComponent = () => {
  const [level, setLevel] = useState<LevelType>(0);
  const [gameCards, setGameCards] = useState<ICard[]>([]);
  const [revealedCards, setRevealedCards] = useState<ICard[]>([]);
  const [selectedCards, setSelectedCards] = useState<number[]>([]);

  useEffect(
    () => {
      if (selectedCards.length === 2) {
        const [cardA, cardB] = selectedCards;
        if (gameCards[cardA] === gameCards[cardB]) {
          setRevealedCards([...revealedCards, gameCards[cardA]]);
          setSelectedCards([]);
        } else {
          setTimeout(() => {
            setSelectedCards([]);
          }, 1000);
        }
      }
    },
    [selectedCards]
  );

  function resetGameParams() {
    setSelectedCards([]);
    setRevealedCards([]);
    setGameCards([]);
  }

  function startGame() {
    const selectedCards: ICard[] = [];
    const deck = getShuffledDeck();

    for (let i = 0; i < GAME_LEVEL_CARD_MAPPING[level]; i++) {
      const card = deck.shift();
      if (card) {
        selectedCards.push(card);
      }
    }

    const result = [...selectedCards, ...selectedCards];
    shuffleArray(result);
    setGameCards(result);
    setSelectedCards([]);
    setRevealedCards([]);
  }

  const gameCompleted =
    gameCards.length > 0 && revealedCards.length === gameCards.length / 2;

  return (
    <div style={{ textAlign: "center" }}>
      <header>
        <Title>Memory Game</Title>
        <GameForm>
          <label htmlFor="levelSelect">Difficulty</label>
          <select
            id="levelSelect"
            value={level}
            onChange={event => {
              setLevel(Number(event.target.value) as LevelType);
            }}
          >
            <option value={0}>Easy</option>
            <option value={1}>Moderate</option>
            <option value={2}>Hard</option>
          </select>
          <button type="button" onClick={() => startGame()}>
            Start Game
          </button>
        </GameForm>
      </header>
      <main>
        {gameCompleted && (
          <VictoryMessage onClick={() => resetGameParams()}>
            You Won!
          </VictoryMessage>
        )}
        {gameCards.map((card, index) => {
          const { suit, rank } = card;
          const hidden =
            (selectedCards.indexOf(index) > -1 ||
              revealedCards.indexOf(card) > -1) === false;
          return (
            <Card
              key={index}
              hidden={hidden}
              suit={suit}
              rank={rank}
              onClick={() => {
                if (hidden && selectedCards.length < 2) {
                  setSelectedCards([...selectedCards, index]);
                }
              }}
            />
          );
        })}
      </main>
    </div>
  );
};

export default Game;
