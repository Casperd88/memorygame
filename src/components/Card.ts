import styled, { css } from "styled-components";
import CardSprite from "./../assets/images/cards.png";
import { ICard } from "./../types";

const CARD_WIDTH = 2178 / 13;
const CARD_HEIGHT = 1216 / 5;

const CARD_ROW_ORDER = ["clubs", "diamonds", "hearts", "spades"];

const Card = styled.div<ICard & { hidden: boolean }>`
  display: inline-block;
  width: ${120}px;
  margin: 10px;
  background-image: url(${CardSprite});
  background-size: ${120 * 13}px auto;
  border-radius: 10px;
  box-shadow: 5px 5px 30px rgba(0, 0, 0, 0.5);
  overflow: hidden;
  ${({ suit, rank, hidden = false }) => {
    const cardHeight = CARD_HEIGHT * (120 / CARD_WIDTH);
    return css`
      ${hidden &&
        css`
          cursor: pointer;
        `}
      height: ${cardHeight}px;
      background-position: ${
        hidden
          ? `-${2 * 120}px -${4 * cardHeight}px`
          : `-${(rank - 1) * 120}px -${cardHeight *
              CARD_ROW_ORDER.indexOf(suit)}px`
      };
    `;
  }}
`;

export default Card;
