import React from "react";
import ReactDOM from "react-dom";
import GlobalStyle from "./components/GlobalStyle";
import Game from "./components/Game";

ReactDOM.render(
  <>
    <GlobalStyle />
    <Game />
  </>,
  document.getElementById("root")
);
