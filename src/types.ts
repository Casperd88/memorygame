export interface ICard {
  suit: "clubs" | "diamonds" | "hearts" | "spades";
  rank: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13;
}

export type LevelType = 0 | 1 | 2;
